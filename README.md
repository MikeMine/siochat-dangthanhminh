# siochat - ĐẶNG THANH MINH - MINF19

This is the project for IOT course of IEI .

## TD1

The main of this project is to build a `tchat` application similar to IRC leveraging on a `client/server` architecture. 

## TD2 

Continue TD1 and add the management of private messages, group management. 

Download ElasticSearch, run and config in project

## TD3

1. Setup `mocha`, `chai` and use as a testing frameworks in pair with socket.io-client to test the behavior of server side .

2. Test all the features of server side

## TD4 

