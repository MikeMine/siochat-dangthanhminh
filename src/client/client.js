/*
    Author: ĐẶNG THANH MINH
    Class: MINF19
    Project: IOT - siochat - TD2
 */

/*
    Event name: packet

    Response in JSON format:
    { sender: who, action: 'join', password: what }
    { sender: who, action: 'broadcast', msg: what }
    { sender: who, action: 'list', userlist: list of users }
    { sender: who, action: 'quit' }

    { sender: who, action: 'send', dest: whom, msg: what }
    { sender: who, action: 'cgroup', group: name }
    { sender: who, action: 'jgroup', group: name }
    { sender: who, action: 'gbroadcast', group: name, msg: what }
    { sender: who, action: 'gmembers', group: name, members: list of members }
    { sender: who, action: 'ghistmsgs', group: name }
    { sender: who, action: 'groups', groups: list of groups }
    { sender: who, action: 'gleave', group: name }

    The first step of establishing secure connection
    { sender: who, action: 'secure_1', dest: whom, public_key: what }
    The second step
    { sender: who, action: 'secure_2', dest: whom, public_key: what }

    After that, we can send an encrypted message along with iv
    { sender: who, action: 'secure_send', dest: whom, encrypted_msg: what, iv: what }
 */

const io = require('socket.io-client');
const socket = io.connect('http://localhost:3000');
const readLine = require('readline');
const crypto = require('crypto');
const rl = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
});

var nickName = null;
var password = null;

/* Pattern for regexr testing */
var patternSend = /^s;([A-Za-z0-9]+);(.+)/i;  //For send private msg
var patternGroupBroadcast = /^bg;([A-Za-z0-9]+);(.+)/i; //For group broadcast
var patternInvite = /^invite;([A-Za-z0-9]+);(.+)/i; //For invite to group
var patternKick = /^kick;([A-Za-z0-9]+);([A-Za-z0-9]+);(.+)/i; //For kick out of group
var patternSecure = /^sss;([A-Za-z0-9]+);(.+)/i; // for sending an encrypted message

/* For secure connection */
var ecdh;
var my_public_key, partner_public_key, secret_key;

//Listen to the 'connect' event that fired upon a successful connection
socket.on('connect', () => {
    /*
    All command-line arguments received by the shell are given to the process in an array called argv
     */
    nickName = process.argv[2];
    password = process.argv[3];
    console.log(`[INFO]: Welcome ${nickName}`);
    socket.emit('packet', {
        'sender': nickName,
        'action': 'join',
        'password': password
    });
});

//Listen to the 'disconnect' event that fired upon a successful disconnect
socket.on('disconnect', (reason) => {
    console.log(`[INFO]: Server disconnected, reason: ${reason}`)

    //Clost the rl instance. When called, the 'close' event will be emitted.
    rl.close();
});

/*
    The 'line' event is emitted whenever the input stram receives an end-of-line (\n, \r or \r\n).
    This usually occurs when the user presses the <Enter> or <Return> keys
 */
rl.on('line', (input) => {
    /*
        The handler function is called with a string containing the single line of received input
     */
    //Broadcast a message: b;Hello
    if(true === input.startsWith('b;')){
        var str = input.slice(2);
        socket.emit('packet', {
            'sender': nickName,
            'action': 'broadcast',
            'msg': str
        });
    }
    //List all nicknames in the chat: ls;
    else if('ls;' === input.trim()){
        socket.emit('packet',{
            'sender': nickName,
            'action': 'list'
        });
    }
    //Quit the chat: q;
    else if('q;' === input.trim()){
        socket.emit('packet',{
            'sender': nickName,
            'action': 'quit'
        });
    }
    //Send a message secretly to someone: s;ted;hello ted
    else if(true === patternSend.test(input.trim())){
        var info = input.match(patternSend);
        socket.emit('packet', {
            'sender': nickName,
            'action': 'send',
            'dest': info[1],
            'msg': info[2]
        });
    }
    //Create a group: cg;<group name>
    else if(true === input.startsWith('cg;')){
        var gname = input.slice(3);
        socket.emit('packet',{
            'sender': nickName,
            'action': 'cgroup',
            'group': gname
        });
    }
    //Join a group: j;<group name>
    else if(true === input.startsWith('j;')){
        var gname = input.slice(2);
        socket.emit('packet',{
            'sender': nickName,
            'action': 'jgroup',
            'group': gname
        });
    }
    //Broadcast a message to a group: bg;friends;hello
    else if(true === patternGroupBroadcast.test(input.trim())){
        var info = input.match(patternGroupBroadcast);
        socket.emit('packet', {
            'sender': nickName,
            'action': 'gbroadcast',
            'group': info[1],
            'msg': info[2]
        });
    }
    //List all clients that are inside a group: members;<group name>
    else if(true === input.startsWith('members;')){
        var gname = input.slice(8);
        socket.emit('packet', {
            'sender': nickName,
            'action': 'gmembers',
            'group': gname
        });
    }
    //List the history of messages in a group
    else if(true === input.startsWith('messages;')){
        var gname = input.slice(9);
        socket.emit('packet', {
            'sender': nickName,
            'action': 'ghistmsgs',
            'group': gname
        });
    }
    //List the existing groups: groups;
    else if('groups;' === input.trim()){
        socket.emit('packet', {
            'sender': nickName,
            'action': 'groups'
        });
    }
    //Leave a group: leave;<group name>
    else if(true === input.startsWith('leave;')){
        var gname = input.slice(6);
        socket.emit('packet', {
            'sender': nickName,
            'action': 'leave',
            'group': gname
        });
    }
    //Invite the user in the group
    else if(true === patternInvite.test(input.trim())){
        var info = input.match(patternInvite);
        socket.emit('packet', {
            'sender': nickName,
            'action': 'invite',
            'group': info[1],
            'dest': info[2]
        });
    }
    //Kick out the user from the group with the reason: kick;<group name>;dest;reason
    else if(true === patternKick.test(input.trim())){
        var info = input.match(patternKick);
        socket.emit('packet', {
            'sender': nickName,
            'action': 'kick',
            'group': info[1],
            'dest': info[2],
            'reason': info[3]
        });
    }
    /* Establish a secure connection with Ted: ss;ted */
    // Share key: https://asecuritysite.com/encryption/js_ecdh
    else if (true === input.startsWith("ss;")) {
        var str = input.slice(3);

        // Create an ecdh instance with key size of 256 bits
        ecdh = crypto.createECDH('secp256k1');

        // Generates private and public key, and returns the public key
        my_public_key = ecdh.generateKeys();

        socket.emit('packet', {
            'sender': nickName,
            'action': 'secure_1',
            'dest': str,
            'public_key': my_public_key});
    }

    /* Send an encrypted message to Ted: sss;ted;hello ted */
    else if (true === patternSecure.test(input)) {
        var info = input.match(patternSecure);

        // Length of secret key is dependent on the algorithm. In this case for aes256,
        // it requires 32 bytes (256 bits), please pay attention that our secret key
        // generated in the establish steps meet this requirement
        const algorithm = 'aes-256-cbc';

        // Initialization vector
        // Block sizes of AES are 128 bits, iv has the same size as the block size
        const randome_iv = crypto.randomBytes(16);

        // Create cipher object with the given arguments
        const cipher = crypto.createCipheriv(algorithm, secret_key, randome_iv);

        // Start to encrypt
        let encrypted = cipher.update(info[2] /* raw message */, 'utf8', 'hex');
        encrypted += cipher.final('hex');

        // Send iv along with the encrypted message for decrypting message
        socket.emit('packet', {
            'sender': nickName,
            'action': 'secure_send',
            'dest': info[1],
            'encrypted_msg': encrypted,
            'iv': randome_iv});
    }
    else {
        console.log('[ERROR]: don\'t support this kind of syntax');
    }

});

//Listen to the 'packet' event that is emitted from the server
socket.on('packet', (packet_data) => {
    switch (packet_data.action) {
        case 'join':
            console.log(`[INFO]: ${packet_data.sender} has joined the chat`);
            break;
        case 'broadcast':
            console.log(`${packet_data.msg}`);
            break;
        case 'list':
            console.log('[INFO]: List of nicknames:');
            for(var i = 0; i < packet_data.userlist.length; i++){
                console.log(packet_data.userlist[i]);
            }
            break;
        case 'quit':
            console.log(`[INFO]: ${packet_data.sender} quit the chat`);
            break;
        case 'send':
            console.log(`${packet_data.msg}`);
            break;
        case 'cgroup':
            /*
            if(packet_data.group != null){
                console.log(`${packet_data.group} created`);
            }
            else{
                console.log(`${packet_data.group} is existed`);
            }
            */
            break;
        case 'jgroup':
            console.log(`[INFO]: ${packet_data.sender} has joined the group`);
            break;
        case 'gbroadcast':
            console.log(`${packet_data.msg}`);
            break;
        case 'gmembers':
            console.log('[INFO]: List of members:');
            for(var i = 0; i < packet_data.members.length; i++){
                console.log(packet_data.members[i]);
            }
            break;
        case 'ghistmsgs':
            console.log('[INFO]: History of messages:');
            for(var i = 0; i < packet_data.msgs.length; i++){
                console.log(packet_data.msgs[i]);
            }
            break;
        case 'groups':
            console.log('[INFO]: List of groups:');
            for(var i = 0; i < packet_data.groups.length; i++){
                console.log(packet_data.groups[i]);
            }
            break;
        case 'leave':
            console.log(`[INFO]: ${packet_data.sender} left the group`);
            break;
        case 'invite':
            console.log(`[INFO]: ${packet_data.dest} has been added into group`);
            break;
        case 'kick':
            console.log(`[INFO]: User ${packet_data.dest} has been removed out of the group ${packet_data.group}, 
            with reason: ${packet_data.reason}`);
            break;

        case 'secure_1':
            partner_public_key = packet_data.public_key;

            // Create an ecdh instance with key size of 256 bits
            ecdh = crypto.createECDH('secp256k1');

            // Generates private and public key, and returns the public key
            my_public_key = ecdh.generateKeys();

            // Send my public key back to the sender
            socket.emit('packet', {
                'sender': nickName,
                'action': 'secure_2',
                'dest': packet_data.sender,
                'public_key': my_public_key});

            // Generates the secret key, please pay attention that this key is the same at two ends
            secret_key = ecdh.computeSecret(partner_public_key);

            // For debugging
            //console.log("[INFO]: Secret key: %s", secret_key.toString('hex'));
            console.log(`[INFO]: Secret key: ${secret_key.toString('hex')}`);
            break;

        case 'secure_2':
            partner_public_key = packet_data.public_key;

            // Generates the secret key, please pay attention that this key is the same at two ends
            secret_key = ecdh.computeSecret(partner_public_key);

            // For debugging
            // console.log("[INFO]: Secret key: %s", secret_key.toString('hex'));
            console.log(`[INFO]: Secret key: ${secret_key.toString('hex')}`);
            break;

        case "secure_send":
            // Use the same algorithm as the sender
            const algorithm = 'aes-256-cbc';

            // Initialization vector from the sender
            const iv = packet_data.iv;

            console.log(`[INFO]: Secret key: ${secret_key.toString('hex')}`);
            console.log(`[INFO]: IV key: ${iv}`);

            // Create decipher object with the given arguments
            const decipher = crypto.createDecipheriv(algorithm, secret_key, iv);

            // Start to decrypt the message from the sender
            let decrypted = decipher.update(packet_data.encrypted_msg, 'hex', 'utf8');
            decrypted += decipher.final('utf8');

            // Show the decrypted message at our end
            console.log(`${decrypted}`);
            break;
        default:
            break;
    }
});

