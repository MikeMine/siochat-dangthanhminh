/*
    Author: ĐẶNG THANH MINH
    Class: MINF19
    Project: IOT - siochat -TD2
 */

/*
    Event name: packet

    Response in JSON format:
    { sender: who, action: 'join' }
    { sender: who, action: 'broadcast', msg: what }
    { sender: who, action: 'list', userlist: list of users }
    { sender: who, action: 'quit' }

    { sender: who, action: 'send', dest: whom, msg: what }
    { sender: who, action: 'cgroup', group: name }
    { sender: who, action: 'jgroup', group: name }
    { sender: who, action: 'gbroadcast', group: name, msg: what }
    { sender: who, action: 'gmembers', group: name, members: list of members }
    { sender: who, action: 'ghistmsgs', group: name, msgs: list of messages }
    { sender: who, action: 'groups', groups: list of groups }
    { sender: who, action: 'gleave', group: name }

    The first step of establishing secure connection
    { sender: who, action: 'secure_1', dest: whom, public_key: what }
    The second step
    { sender: who, action: 'secure_2', dest: whom, public_key: what }

    After that, we can send an encrypted message along with iv
    { sender: who, action: 'secure_send', dest: whom, encrypted_msg: what, iv: what }
 */

const port = 3000;
const io = require('socket.io')(port);

/*
    [
        {name: roni, id: xxxxx},
        {name: ted, id: xxxxx}
    ]
 */
//var users = [];

//Load 'elasticsearch' module
const elasticSearch = require('@elastic/elasticsearch');
const es_client = new elasticSearch.Client({node: 'http://localhost:9200'});

//Load bcrypt module
const bcrypt = require('bcrypt');
const saltRounds = 10;

console.log(`Server is listening on port: ${port}`);

//Listen to the 'connect' event that fired upon a successful connection
io.on('connect', (socket) => {
    console.log('A user connected');

    //Listen to the 'disconnect' event that fired upon a successful disconnection
    socket.on('disconnect', (reason) => {
        console.log(`A user disconnected, reason: ${reason}`);

        //Remove the user that disconnected
        //users.splice(users.findIndex(v => v.id == socket.id), 1);
        console.log(`Numbers of users: ${io.of('/').server.engine.clientsCount}`);
    });

    //Listen to 'packet' event from an individual socket
    socket.on('packet', (packet_data) => {
        switch (packet_data.action) {
            case 'join':
                console.log(`Name: ${packet_data.sender}, ID: ${socket.id}`);
                console.log(`Numbers of users: ${io.of('/').server.engine.clientsCount}`);

                //Save nickname to this socket object
                //socket.nickname = packet_data.sender;
                /* Check if a nickname exists */
                /*  index: accounts
                    documents in the index:
                        { "nickname": "ted", "password": "hi" }
                        { "nickname": "roni", "password": "hi" }
                */
                es_client.search({
                    index: 'accounts',

                    /* Search queries */
                    body: {
                        'query': {
                            'match': {
                                'nickname': packet_data.sender
                            }
                        }
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (404 === result.statusCode ||
                        /* Index not found, this means this is the first time that a user join,
                            so just save the account information */

                        (200 === result.statusCode && null === result.body.hits.max_score)) {
                        /* Nickname not found, this means this is the first time that a user join,
                            so just save the account information */

                        if (404 === result.statusCode) {
                            console.log('Index not found!');
                        }
                        else {
                            console.log('Nickname not found!');
                        }

                        bcrypt.hash(packet_data.password, saltRounds, function(err, hashed_password) {
                            /* Adds a JSON document to the specified index and makes it searchable.
                            If the document already exists, updates the document and increments its version.*/
                            es_client.create({
                                /* Document ID */
                                id: new Date().getTime(),

                                /* By default, the index is created automatically if it doesn’t exist */
                                index: 'accounts',

                                /* If true then refresh the affected shards to make this operation visible
                                to search */
                                refresh: 'true',

                                /* Document data */
                                body: {
                                    'nickname': packet_data.sender,
                                    'password': hashed_password
                                }
                            }, (err, result) => {
                                if (err) console.log(err);
                                if (201 === result.statusCode) {
                                    console.log('Saved the account to the database');
                                }
                            });
                        });

                    } else {
                        /* Found the nickname, check if the password matches or not */
                        bcrypt.compare(packet_data.password, result.body.hits.hits[0]._source.password,
                            function(err, result) {
                                if (true === result) {
                                    console.log('Correct password');

                                    /*  Broadcasting means sending a packet to everyone else
                                    except for the socket that starts it */
                                    socket.broadcast.emit('packet', packet_data);
                                } else {
                                    console.log('Incorrect password!');
                                    socket.disconnect(true);
                                }
                            });
                    }
                });

                //Broadcasting means sending a packet to everyone else except for the socket that starts it
                //socket.broadcast.emit('packet', packet_data);

                //Save nickname to this socket object
                socket.nickname = packet_data.sender;
                break;
            case 'broadcast':
                //Broadcasting means sending a packet to everyone else except for the socket that starts it
                socket.broadcast.emit('packet', packet_data);

                /*
                Save all messages to elasticsearch
                    index: room_messages
                    documents in the index:
                        { 'group': socket.id, 'sender': ted, 'msg': 'hi' }
                        { 'group': socket.id, 'sender': roni, 'msg': 'hi' }
                 */
                /*
                Add a JSON document to the specified index and makes it searchable.
                If the document already exists, updates the document and increments its version.
                 */
                es_client.create({
                    //Document ID
                    id: new Date().getTime(),

                    //By default, the index is created automatically if it doesn't exist
                    index: 'room_messages',

                    //If true then refresh the affected shards to make this operation visible to search
                    refresh: 'true',

                    //Document data
                    body: {
                        'group': socket_id,
                        'sender': packet_data.sender,
                        'msg': packet_data.msg
                    }
                }, (err, result) => {
                    if(err)
                        console.log(err);

                    if(201 === result.statusCode){
                        console.log('Saved the message to the database');
                    }
                });
                break;
            case 'list':
                var users = [];

                for(var key in io.of('/').sockets){
                    users.push(io.of('/').sockets[key].nickname);
                }
                //Sending a packet to socket that starts it
                socket.emit('packet',{
                   'sender': packet_data.sender,
                    'action': 'list',
                    'userlist': users
                });
                break;
            case 'quit':
                //Broadcasting means sending a packet to everyone else except for the socket that starts it
                socket.broadcast.emit('packet', packet_data);
                socket.disconnect(true);
                break;
            case 'send':
                var socket_id = null;

                //Search all nicknames
                for(var key in io.of('/').sockets){
                    if(packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname){
                        socket_id = io.of('/').sockets[key].id;
                        break;
                    }
                }
                if(socket_id !== null){
                    /*
                    Each socket is identified by a random, unguessable, unique identifier Socket#id.
                    For your convenience, each socket automatically joins a room identified by its own id.
                     */
                    io.to(socket_id).emit('packet', packet_data);

                    /*
                    Save all messages to elasticsearch
                        index: room_messages
                        documents in the index:
                            { 'group': socket.id, 'sender': ted, 'msg': 'hi' }
                            { 'group': socket.id, 'sender': roni, 'msg': 'hi' }
                     */
                    /*
                    Add a JSON document to the specified index and makes it searchable.
                    If the document already exists, updates the document and increments its version.
                     */
                    es_client.create({
                        //Document ID
                        id: new Date().getTime(),
                        //By default, the index is created automatically if it doesn't exist
                        index: 'room_messages',
                        //If true then refresh the affected shards to make this operation visible to search
                        refresh: 'true',
                        //Document data
                        body: {
                            'group': socket_id,
                            'sender': packet_data.sender,
                            'msg': packet_data.msg
                        }
                    }, (err, result) => {
                        if(err)
                            console.log(err);
                        if(201 === result.statusCode){
                            console.log('Saved the message to the database');
                        }
                    });
                }
                break;
            case 'cgroup':
                /*
                const rooms = io.of('/').adapter.rooms;
                if(rooms.length <= 0){
                    rooms.push(packet_data.group);
                }
                else{
                    if(rooms.contains(packet_data.group)){
                        packet_data.group = null;
                    }
                    else{
                        rooms.push(packet_data.group);
                    }
                }
                socket.emit('packet', packet_data);
                */
                console.log('need?');
                break;
            case 'jgroup':
                socket.join(packet_data.group, () => {
                    console.log(`Group: ${packet_data.group}, Joined: ${packet_data.sender}`);

                    //Sending to all clients in a group, including sender
                    //io.to(packet_data.group).emit('packet', packet_data);
                    socket.to(packet_data.group).emit('packet', packet_data);
                });
                break;
            case 'gbroadcast':
                //Sending to all clients in a group except sender
                socket.to(packet_data.group).emit('packet', packet_data);

                /*
                Save the message in group
                room_messages:{
                    aaa: ["hi", "hello"],
                    bbb: ["abc", "xyz"]
                }
                 */
                /*
                if(undefined === io.of('/').room_messages){
                    io.of('/').room_messages = {};
                }

                if(undefined === io.of('/').room_messages[packet_data.group]){
                    io.of('/').room_messages[packet_data.group] = [];
                }
                io.of('/').room_messages[packet_data.group].push(packet_data.msg);

                 */

                /*
                    Save all messages to elasticsearch
                        index: room_messages
                        documents in the index:
                            { 'group': socket.id, 'sender': ted, 'msg': 'hi' }
                            { 'group': socket.id, 'sender': roni, 'msg': 'hi' }
                     */
                /*
                Add a JSON document to the specified index and makes it searchable.
                If the document already exists, updates the document and increments its version.
                 */
                es_client.create({
                    //Document ID
                    id: new Date().getTime(),

                    //By default, the index is created automatically if it doesn't exist
                    index: 'room_messages',

                    //If true then refresh the affected shards to make this operation visible to search
                    refresh: 'true',

                    //Document data
                    body: {
                        'group': packet_data.group,
                        'sender': packet_data.sender,
                        'msg': packet_data.msg
                    }
                }, (err, result) => {
                    if(err)
                        console.log(err);

                    if(201 === result.statusCode){
                        console.log('Saved the message to the database');
                    }
                });
                break;
            case 'gmembers':
                var members = [];

                io.of('/').in(packet_data.group).clients((error, clients) => {
                    if(error)
                        throw error;

                    //Clients are an array of socket ids in this group
                    for(var i = 0; i < clients.length; i++){
                        members.push(io.of('/').sockets[clients[i]].nickname);
                    }

                    //Sending a packet to socket that starts it
                    socket.emit('packet',{
                        'sender': packet_data.sender,
                        'action': 'gmembers',
                        'group': packet_data.group,
                        'members': members
                    });
                });

                break;
            case 'ghistmsgs':
                //var msgs = io.of('/').room_messages[packet_data.group];
                /*
                room_messages:{
                    aaa: ["hi", "hello"],
                    bbb: ["abc", "xyz"]
                }
                 */

                var msgs = [];
                /*
                    index: room_messages
                    documents in the index:
                        { 'group': socket.id, 'sender': ted, 'msg': 'hi' }
                        { 'group': socket.id, 'sender': roni, 'msg': 'hi' }
                 */
                es_client.search({
                    index: 'room_messages',

                    //Search queries
                    body:{
                        'query': {
                            "match": {
                                "group": packet_data.group
                            }
                        }
                    }
                }, (err, result) => {
                    if(err)
                        console.log(err);

                    for(var i = 0; i < result.body.hits.hits.length; i++){
                        msgs.push(result.body.hits.hits[i]._source.sender + ": " +
                            result.body.hits.hits[i]._source.msg);
                    }

                    //Sending a packet to socket that starts it
                    socket.emit('packet', {
                        'sender': packet_data.sender,
                        'action': 'ghistmsgs',
                        'group': packet_data.group,
                        'msgs': msgs
                    });
                });
                break;
            case 'groups':
                var rooms = io.of('/').adapter.rooms;

                //Sending a packet to socket that starts it
                socket.emit('packet',{
                    'sender': packet_data.sender,
                    'action': 'groups',
                    'groups': Object.keys(rooms)
                });
                break;
            case 'leave':
                socket.leave(packet_data.group, () => {
                    console.log(`Group: ${packet_data.group}, Left: ${packet_data.sender}`);

                    //Sending to all clients in a group
                    io.to(packet_data.group).emit('packet', packet_data);
                });
                break;
            case 'invite':
                var socket_id = null;

                //Search all nicknames
                for(var key in io.of('/').sockets){
                    if(packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname){
                        socket_id = io.of('/').sockets[key].id;
                        break;
                    }
                }
                if(socket_id !== null){
                    io.sockets.connected[socket_id].join(packet_data.group);
                    io.to(packet_data.group).emit('packet', packet_data);
                }
            case 'kick':
                var socket_id = null;

                //Search all nicknames
                for(var key in io.of('/').sockets){
                    if(packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname){
                        socket_id = io.of('/').sockets[key].id;
                        break;
                    }
                }
                if(socket_id !== null){
                    io.sockets.connected[socket_id].leave(packet_data.group, () => {
                        console.log(`[INFO]: User ${packet_data.dest} left the group ${packet_data.group}`);
                        io.to(packet_data.group).emit('packet', packet_data);
                    });
                }
                break;
            default:
                break;
        }
    });
});

